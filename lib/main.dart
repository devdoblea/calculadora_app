import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:math_expressions/math_expressions.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
	    debugShowCheckedModeBanner: false,
      title: 'Calculadora Flutter',
      home: MainPage(),
    );
  }
}

String strInput = "";
final textControllerInput  = TextEditingController();
final textControllerResult = TextEditingController();

class MainPage extends StatefulWidget {
  //
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  //
  @override
  void initState() {
    super.initState();
    textControllerInput.addListener(() {});
    textControllerResult.addListener(() {});
  }

  @override
  void dispose() {
  	textControllerInput.dispose();
  	textControllerResult.dispose();
  	super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: Color.fromARGB(200, 126, 87, 194),
      ),
    );
    return Scaffold(
      body: Container(
        color: Color.fromARGB(200, 126, 87, 194),
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Column(
          children: <Widget>[
            Expanded(
              flex: 1,
              child: ClipPath(
                clipper: BottomWaveClipper(),
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  color: Colors.white,
                  child: Column(
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: SizedBox(),
                      ),
                      Expanded(
                        flex: 1,
                        child: Container(
                          padding: EdgeInsets.all(10.0),
                          alignment: Alignment.centerRight,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: <Widget>[
	                            new Padding(
																padding: EdgeInsets.symmetric(horizontal: 10.0),
																child: new TextField(
																	decoration: new InputDecoration.collapsed(
																		hintText: "0",
																		hintStyle: TextStyle(
																			fontSize: 30.0,
																			fontFamily: 'RobotoMono',
																		)),
				                            style: TextStyle(
																			fontSize: 30.0,
					                            fontFamily: 'RobotoMono',
				                            ),
				                            textAlign: TextAlign.right,
				                            controller: textControllerInput,
				                            onTap: () =>
						                            FocusScope.of(context).requestFocus(new FocusNode()),
			                            )
	                            ),
	                            SizedBox( height: 10.0),
	                            new Padding(
																padding: EdgeInsets.symmetric(horizontal: 10.0),
																child: TextField(
																	decoration: new InputDecoration.collapsed(
																		hintText: "Result",
																		// fillColor: Colors.deepPurpleAccent,
						                        hintStyle: TextStyle(fontFamily: 'RobotoMono')),
				                            textInputAction: TextInputAction.none,
				                            keyboardType: TextInputType.number,
				                            style: TextStyle(
						                            fontSize: 32.0,
						                            fontFamily: 'RobotoMono',
						                            fontWeight: FontWeight.bold
					                            // color: Colors.deepPurpleAccent
				                            ),
				                            textAlign: TextAlign.right,
				                            controller: textControllerResult,
				                            onTap: () {
					                            FocusScope.of(context).requestFocus(new FocusNode());
					                            // ClipboardManager.copyToClipBoard(textControllerResult.text).then((result) {
					                            //   Fluttertoast.showToast(
					                            //       msg: "Value copied to clipboard!",
					                            //       toastLength: Toast.LENGTH_SHORT,
					                            //       gravity: ToastGravity.CENTER,
					                            //       timeInSecForIos: 1,
					                            //       backgroundColor: Colors.blueAccent,
					                            //       textColor: Colors.white,
					                            //       fontSize: 16.0
					                            //   );
					                            // });
				                            },
			                            )
	                            ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Container(
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.all(10.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Expanded(flex: 1, child: myButtonAC("AC")),
                          Expanded(flex: 1, child: myButton("7")),
                          Expanded(flex: 1, child: myButton("4")),
                          Expanded(flex: 1, child: myButton("1")),
                          Expanded(flex: 1, child: myButton("%")),
                        ],
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Expanded(flex: 1, child: myIconButton(Icons.backspace)),
                          Expanded(flex: 1, child: myButton("8")),
                          Expanded(flex: 1, child: myButton("5")),
                          Expanded(flex: 1, child: myButton("2")),
                          Expanded(flex: 1, child: myButton("0")),
                        ],
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Expanded(flex: 1, child: myButton("/")),
                          Expanded(flex: 1, child: myButton("9")),
                          Expanded(flex: 1, child: myButton("6")),
                          Expanded(flex: 1, child: myButton("3")),
                          Expanded(flex: 1, child: myButton(".")),
                        ],
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Expanded(flex: 0, child: myButton("+")),
                          Expanded(flex: 0, child: myButton("-")),
                          Expanded(flex: 0, child: myButton("*")),
                          Expanded(flex: 1, child: myButtonEqual("=")),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget myIconButton(IconData icon) {
    return Container(
		  child: FlatButton(
		    onPressed: () {
	      	textControllerInput.text =
		      (textControllerInput.text.length > 0) ? (textControllerInput.text.substring(0, textControllerInput.text.length - 1)) : "";
		    },
			  child: Icon(icon, color: Colors.white, size: 30.0),
			  color: Colors.transparent,
			  splashColor: Colors.black,
			  shape: CircleBorder(),
	    ),
    );
  }

  Widget myButtonAC(String name) {
		return Container(
			child: FlatButton(
				onPressed: () {
					setState(() {
						textControllerInput.text = "";
						textControllerResult.text = "";
					});
				},
				child: Text(
					name,
					style: TextStyle(
						fontSize: 22.0, color: Colors.white, fontFamily: 'RobotoMono'
					),
				),
				color: Colors.transparent,
				splashColor: Colors.black,
				shape: CircleBorder(),
			),
		);
  }

  Widget myButton(String name) {
    return Container(
	    child: FlatButton(
		    onPressed: () {
		    	setState(() {
		    		textControllerInput.text = textControllerInput.text + name;
		    	});
	      },
		    child: Text(
			    name,
			    style: TextStyle(
						fontSize: 22.0, color: Colors.white, fontFamily: 'RobotoMono'
					),
		    ),
	      color: Colors.transparent,
	      padding: EdgeInsets.all(18.0),
	      splashColor: Colors.black,
	      shape: CircleBorder(),
	    ),
    );
  }

  Widget myButtonEqual(name) {
  	return Container(
			child: RaisedButton(
				color: Colors.transparent,
				onPressed: () {
					// Calcular lo que introduce el usuario
					// Primero Parseamos la expresion
					Parser p = new Parser();
					ContextModel cm = new ContextModel();
					Expression exp = p.parse(textControllerInput.text);
					// Luego se evalua y calcula lo expresado
					// y se envia el resultado a la pantalla del usuario
					setState(() {
						textControllerResult.text = exp.evaluate(EvaluationType.REAL, cm).toString();
					});
				},
				child: Text(
					name,
					style: TextStyle(
						fontSize: 45.0, color: Colors.white, fontFamily: 'RobotoMono'
					),
				),
			)
		);
	}
}

class BottomWaveClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    //
    var path = new Path();
    path.lineTo(0.0, size.height - 20);

    var firstControlPoint = Offset(size.width / 4, size.height);
    var firstEndPoint = Offset(size.width / 2.25, size.height - 30.0);
    path.quadraticBezierTo(
		    firstControlPoint.dx,
		    firstControlPoint.dy,
		    firstEndPoint.dx,
		    firstEndPoint.dy
    );

    var secondControlPoint = Offset(size.width - (size.width / 3.25), size.height - 65);
    var secondEndPoint = Offset(size.width, size.height - 40);
    path.quadraticBezierTo(
		    secondControlPoint.dx,
		    secondControlPoint.dy,
		    secondEndPoint.dx,
		    secondEndPoint.dy
    );

    path.lineTo(size.width, size.height - 40);
    path.lineTo(size.width, 0.0);

    path.close();

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    //
    return true;
  }
} 
